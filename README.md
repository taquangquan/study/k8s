### Course
    https://youtube.com/playlist?list=PLwJr0JSP7i8D-QS50lYsXpAg-jYoqxMVy

### Setup cluster

##### Init VMs
    vargrant up

##### Init master node
    master> kubeadm init --apiserver-advertise-address=172.16.10.100 --pod-network-cidr=192.168.0.0/16

    master> mkdir -p $HOME/.kube
    master> sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    master> sudo chown $(id -u):$(id -g) $HOME/.kube/config

    master> kubectl apply -f https://docs.projectcalico.org/v3.10/manifests/calico.yaml

##### Connect worker nodes to master node
    master> kubeadm token create --print-join-command 
    workerX> # Run command generated above

##### Add cluster config to kubectl on host machine
    host> scp root@172.16.10.100:/etc/kubernetes/admin.conf ~/.kube/config-mycluster
    host> export KUBECONFIG=~/.kube/config:~/.kube/config-mycluster
    host> kubectl config view --flatten > ~/.kube/config_temp
    host> mv ~/.kube/config_temp ~/.kube/config

    host> kubectl config get-contexts
    host> kubectl config use-context kubernetes-admin@kubernetes

### Practice
    kubectl cluster-info
    kubectl get nodes
    kubectl get pods -A
    kubectl config view
    watch kubectl get all -o wide
